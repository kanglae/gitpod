# Sentry

## Setup

```
gitpod /workspace/gitpod-k3s/hands-on/sentry (main) $ yarn init
yarn init v1.22.19
question name (sentry): 
question version (1.0.0): 
question description: 
question entry point (index.js): 
question repository url: 
question author: 
question license (MIT): 
question private: 
success Saved package.json
Done in 13.37s.
gitpod /workspace/gitpod-k3s/hands-on/sentry (main) $ yarn add @sentry/node
yarn add v1.22.19
info No lockfile found.
[1/4] Resolving packages...
[2/4] Fetching packages...
[3/4] Linking dependencies...
[4/4] Building fresh packages...
success Saved lockfile.
success Saved 3 new dependencies.
info Direct dependencies
└─ @sentry/browser@7.68.0
info All dependencies
├─ @sentry-internal/tracing@7.68.0
├─ @sentry/browser@7.68.0
└─ @sentry/replay@7.68.0
Done in 2.02s.
gitpod /workspace/gitpod-k3s/hands-on/sentry (main) $ ls
node_modules  package.json  yarn.lock

## trigger

`yarn run start`
